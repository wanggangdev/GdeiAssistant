package edu.gdei.gdeiassistant.Constant;

public class OptionConstantUtils {

    public static final String[] GENDER_OPTIONS = {"未选择", "男", "女", "自定义"};

    public static final String[] DEGREE_OPTIONS = {"未选择", "小学", "初中", "中专/职高/技校", "高中"
            , "专科", "本科", "硕士", "博士"};

    public static final String[] FACULTY_OPTIONS = {"未选择", "教育学院", "政法系", "中文系", "数学系", "外语系"
            , "物理与信息工程系", "化学系", "生物与食品工程学院", "体育学院", "美术学院", "计算机科学系", "音乐系"
            , "教师研修学院", "成人教育学院", "网络教育学院", "马克思主义学院"};
}
